package ffufm.patrick.api

import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.security.SpringSecurityAuditorAware
import ffufm.patrick.api.repositories.user.UserAddressRepository
import ffufm.patrick.api.repositories.user.UserContactDetailRepository
import ffufm.patrick.api.repositories.user.UserUserRepository
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@ActiveProfiles("test")
@SpringBootTest(classes = [SBPatrick::class, SpringSecurityAuditorAware::class])
@AutoConfigureMockMvc
abstract class PassTestBase {
    @Autowired
    private lateinit var userUserRepository: UserUserRepository

    @Autowired
    private lateinit var userContactDetailRepository: UserContactDetailRepository

    @Autowired
    private lateinit var userAddressRepository: UserAddressRepository


    @Autowired
    lateinit var context: ApplicationContext

    @Before
    fun initializeContext() {
        SpringContext.context = context
    }

    @After
    fun cleanRepository(){
        userAddressRepository.deleteAll()
        userContactDetailRepository.deleteAll()
        userUserRepository.deleteAll()
    }
}
