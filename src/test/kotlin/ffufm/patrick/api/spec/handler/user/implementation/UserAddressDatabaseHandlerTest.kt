package ffufm.patrick.api.spec.handler.user.implementation

import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.user.UserAddressRepository
import ffufm.patrick.api.spec.dbo.user.UserAddress
import ffufm.patrick.api.spec.dbo.user.UserAddressDTO
import ffufm.patrick.api.spec.handler.user.UserAddressDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class UserAddressDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var userAddressRepository: UserAddressRepository

    @Autowired
    lateinit var userAddressDatabaseHandler: UserAddressDatabaseHandler

    @Before
    @After
    fun cleanRepositories() {
        userAddressRepository.deleteAll()
    }

    @Test
    fun `test update`() = runBlocking {
        val body: UserAddressDTO = UserAddressDTO()
        val id: Long = 0
        userAddressDatabaseHandler.update(body, id)
        Unit
    }

    @Test
    fun `test remove`() = runBlocking {
        val id: Long = 0
        userAddressDatabaseHandler.remove(id)
        Unit
    }

    @Test
    fun `test create`() = runBlocking {
        val body: UserAddressDTO = UserAddressDTO()
        val id: Long = 0
        userAddressDatabaseHandler.create(body, id)
        Unit
    }
}
