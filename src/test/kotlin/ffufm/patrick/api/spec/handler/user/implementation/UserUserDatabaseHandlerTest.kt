package ffufm.patrick.api.spec.handler.user.implementation

import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.user.UserUserRepository
import ffufm.patrick.api.spec.dbo.user.UserUser
import ffufm.patrick.api.spec.dbo.user.UserUserDTO
import ffufm.patrick.api.spec.handler.user.UserUserDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class UserUserDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var userUserRepository: UserUserRepository

    @Autowired
    lateinit var userUserDatabaseHandler: UserUserDatabaseHandler

    @Before
    @After
    fun cleanRepositories() {
        userUserRepository.deleteAll()
    }

    @Test
    fun `test getById`() = runBlocking {
        val id: Long = 0
        userUserDatabaseHandler.getById(id)
        Unit
    }

    @Test
    fun `test create`() = runBlocking {
        val body: UserUserDTO = UserUserDTO()
        userUserDatabaseHandler.create(body)
        Unit
    }

    @Test
    fun `test update`() = runBlocking {
        val body: UserUserDTO = UserUserDTO()
        val id: Long = 0
        userUserDatabaseHandler.update(body, id)
        Unit
    }

    @Test
    fun `test getAll`() = runBlocking {
        val maxResults: Int = 100
        val page: Int = 0
        userUserDatabaseHandler.getAll(maxResults, page)
        Unit
    }

    @Test
    fun `test remove`() = runBlocking {
        val id: Long = 0
        userUserDatabaseHandler.remove(id)
        Unit
    }

    @Test
    fun `getById should return a user`() = runBlocking {
        val user = UserUser(
            firstName = "Brandon",
            lastName = "Cruz",
            email = "brandon@brandon.com",
            userType = "GC"
        )
        val savedUser = userUserRepository.save(user)
        userUserDatabaseHandler.getById(savedUser.id!!)

        assertEquals(1, userUserRepository.findAll().size)
    }
}
