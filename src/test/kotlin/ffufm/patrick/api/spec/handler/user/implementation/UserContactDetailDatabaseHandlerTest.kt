package ffufm.patrick.api.spec.handler.user.implementation

import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.user.UserContactDetailRepository
import ffufm.patrick.api.spec.dbo.user.UserContactDetail
import ffufm.patrick.api.spec.dbo.user.UserContactDetailDTO
import ffufm.patrick.api.spec.handler.user.UserContactDetailDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class UserContactDetailDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var userContactDetailRepository: UserContactDetailRepository

    @Autowired
    lateinit var userContactDetailDatabaseHandler: UserContactDetailDatabaseHandler

    @Before
    @After
    fun cleanRepositories() {
        userContactDetailRepository.deleteAll()
    }

    @Test
    fun `test createContactDetail`() = runBlocking {
        val body: UserContactDetailDTO = UserContactDetailDTO()
        val id: Long = 0
        userContactDetailDatabaseHandler.createContactDetail(body, id)
        Unit
    }

    @Test
    fun `test removeContactDetail`() = runBlocking {
        val id: Long = 0
        userContactDetailDatabaseHandler.removeContactDetail(id)
        Unit
    }

    @Test
    fun `test updateContactDetail`() = runBlocking {
        val id: Long = 0
        userContactDetailDatabaseHandler.updateContactDetail(id)
        Unit
    }
}
