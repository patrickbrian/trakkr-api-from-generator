package ffufm.patrick.api.spec.handler.user

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.patrick.api.spec.dbo.user.UserAddressDTO
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface UserAddressDatabaseHandler {
    /**
     * Update the Address: Updates an existing Address
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: UserAddressDTO, id: Long): UserAddressDTO

    /**
     * Delete Address by id.: Deletes one specific Address.
     * HTTP Code 200: Successfully deleted address
     */
    suspend fun remove(id: Long)

    /**
     * Create Address: Creates a new Address object
     * HTTP Code 201: The created Address
     */
    suspend fun create(body: UserAddressDTO, id: Long): UserAddressDTO
}

@Controller("user.Address")
class UserAddressHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: UserAddressDatabaseHandler

    /**
     * Update the Address: Updates an existing Address
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/users/addresses/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: UserAddressDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }

    /**
     * Delete Address by id.: Deletes one specific Address.
     * HTTP Code 200: Successfully deleted address
     */
    @RequestMapping(value = ["/users/addresses/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Create Address: Creates a new Address object
     * HTTP Code 201: The created Address
     */
    @RequestMapping(value = ["/users/{id:\\d+}/addresses/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: UserAddressDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body, id) }
    }
}
