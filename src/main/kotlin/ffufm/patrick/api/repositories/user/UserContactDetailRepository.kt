package ffufm.patrick.api.repositories.user

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.patrick.api.spec.dbo.user.UserContactDetail
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface UserContactDetailRepository : PassRepository<UserContactDetail, Long> {
    @Query(
        "SELECT t from UserContactDetail t LEFT JOIN FETCH t.user",
        countQuery = "SELECT count(id) FROM UserContactDetail"
    )
    fun findAllAndFetchUser(pageable: Pageable): Page<UserContactDetail>
}
