package ffufm.patrick.api.handlerimpl.user

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.patrick.api.repositories.user.UserContactDetailRepository
import ffufm.patrick.api.spec.dbo.user.UserContactDetail
import ffufm.patrick.api.spec.dbo.user.UserContactDetailDTO
import ffufm.patrick.api.spec.handler.user.UserContactDetailDatabaseHandler
import kotlin.Long
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("user.UserContactDetailHandler")
class UserContactDetailHandlerImpl : PassDatabaseHandler<UserContactDetail,
        UserContactDetailRepository>(), UserContactDetailDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Successfully added Contact Detail
     */
    override suspend fun createContactDetail(body: UserContactDetailDTO, id: Long): UserContactDetailDTO {
        TODO("not checked yet")
        return repository.save(body.toEntity()).toDto()
    }

    /**
     * : 
     * HTTP Code 200: Successfully deleted the contact detail
     */
    override suspend fun removeContactDetail(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        TODO("not checked yet - update the values you really want updated")
        return repository.delete(original)
    }

    /**
     * : 
     * HTTP Code 200: Successfully updated
     */
    override suspend fun updateContactDetail(id: Long): UserContactDetailDTO {
        val original = repository.findById(id).orElseThrow404(id)
        TODO("not checked yet - update the values you really want updated")
        return repository.save(original).toDto()
    }
}
