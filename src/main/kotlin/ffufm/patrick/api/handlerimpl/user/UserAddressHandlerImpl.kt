package ffufm.patrick.api.handlerimpl.user

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.patrick.api.repositories.user.UserAddressRepository
import ffufm.patrick.api.spec.dbo.user.UserAddress
import ffufm.patrick.api.spec.dbo.user.UserAddressDTO
import ffufm.patrick.api.spec.handler.user.UserAddressDatabaseHandler
import kotlin.Long
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("user.UserAddressHandler")
class UserAddressHandlerImpl : PassDatabaseHandler<UserAddress, UserAddressRepository>(),
        UserAddressDatabaseHandler {
    /**
     * Update the Address: Updates an existing Address
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: UserAddressDTO, id: Long): UserAddressDTO {
        val original = repository.findById(id).orElseThrow404(id)
        TODO("not checked yet - update the values you really want updated")
        return repository.save(original).toDto()
    }

    /**
     * Delete Address by id.: Deletes one specific Address.
     * HTTP Code 200: Successfully deleted address
     */
    override suspend fun remove(id: Long){
        val original = repository.findById(id).orElseThrow404(id)
        TODO("not checked yet - update the values you really want updated")
        return repository.delete(original)
    }

    /**
     * Create Address: Creates a new Address object
     * HTTP Code 201: The created Address
     */
    override suspend fun create(body: UserAddressDTO, id: Long): UserAddressDTO {
        TODO("not checked yet")
        return repository.save(body.toEntity()).toDto()

    }
}
